import cv2
import numpy as np
import torch
from flask import Flask
from flask_compress import Compress
from flask_json import as_json, FlaskJSON
from flask_cors import CORS
from flask_restful import request
from gevent.pywsgi import WSGIServer

from src.inference import Wrapper


def binary_to_cv2(binary_img: bytes) -> np.ndarray:
    nparr = np.frombuffer(binary_img, np.uint8)
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)[..., ::-1]
    return img


def build_app() -> Flask:
    """
    Build Flask app using config
    :param config_path: path to config
    :return: Flask app
    """

    model = Wrapper()

    app = Flask(__name__)

    @app.route("/ping", methods=['GET'])
    @as_json
    def ping():
        return {'ok': True}

    @app.route("/predict", methods=['POST'])
    @as_json
    def predict():
        image_key = next(request.files.keys())
        data = request.files[image_key]

        image_data = data.stream.read()

        img = binary_to_cv2(image_data)

        class1, class2, class3 = model(img)

        return {'root': class1,
                'vowel': class2,
                'consonant': class3}

    FlaskJSON(app)
    CORS(app)
    Compress().init_app(app)
    return app


def run_server():
    app = build_app()
    app.run(host='0.0.0.0', port='7707')
    #
    # http_server = WSGIServer(('0.0.0.0', '7707'), app)
    # http_server.serve_forever()


if __name__ == '__main__':
    run_server()
