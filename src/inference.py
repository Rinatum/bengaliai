from src.models.efficientnet import EfficientNet

import torch
import cv2
import numpy as np


def img_to_tensor(img):
    """
    Converts image to tensor
    :param img: np.ndarray of shape (H, W, 3) representing RGB image either in np.uint8 or np.float type
    :param normalize: normalization parameters. If not None, the must be a dictionary with keys mean, std
    :return: torch.Tensor of shape (3, H, W) of type torch.Float representing RGB image
    (values range from 0 to 1 and normalized according to normalization parameters)
    """
    tensor = torch.from_numpy(np.moveaxis(img / (255.0 if img.dtype == np.uint8 else 1), -1, 0).astype(np.float32))
    return tensor


class Wrapper:
    def __init__(self, model_path='/app/models/effnetb1.pth'):
        self.model = EfficientNet.from_name('efficientnet-b1')

        self.model.load_state_dict(torch.load(model_path, map_location=torch.device('cpu')))

    def __call__(self, img):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        tensor = img_to_tensor(cv2.resize(gray, (64, 64))).unsqueeze(0).unsqueeze(0)
        with torch.no_grad():
            y1, y2, y3 = self.model(tensor)

            vowel = y1.argmax(-1).view(-1).cpu().item()
            root = y2.argmax(-1).view(-1).cpu().item()
            consonant = y3.argmax(-1).view(-1).cpu().item()

        return vowel, root, consonant
