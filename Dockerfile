# FROM nvidia/cuda:10.0-cudnn7-runtime-ubuntu18.04
#FROM tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7
FROM python:3

# Install libraries required by opencv
RUN set -ex; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        libsm6 \
        libxext6 \
        libglib2.0-0 \
        libxrender-dev; \
    apt-get install -y python3-pip && \
    update-alternatives --install /usr/bin/python python /usr/bin/python3 1 && \
    update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1 && \
    rm -rf /var/lib/apt/lists/*;

WORKDIR /app
COPY requirements.txt .
RUN  pip install -r requirements.txt
RUN pip install torch torchvision --no-cache-dir -f https://download.pytorch.org/whl/torch_stable.html
COPY . .
ENV PYTHONPATH /app
EXPOSE 7707
ENV TESTING false

CMD ["python", "api.py"]